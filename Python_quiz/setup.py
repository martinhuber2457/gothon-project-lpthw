try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Instructional quiz',
    'author': 'Martin Huber',
    'url': 'NONE',
    'download_url': 'NONE',
    'author_email': 'martinhuber2457@yahoo.com',
    'version': '2.7.0a1',
    'install_requires': ['nose'],
    'packages': ['I_did_this_task'],
    'scripts': ['bin/yoyo'],
    'name': 'Python_quiz'
}

setup(**config)
